#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set sw=4 et tw=79:

from __future__ import unicode_literals

mpd_lib_available = False

try:
    import mpd
    mpd_lib_available = True

except ImportError:
    pass

import sys


class Mpd(object):
    """
    Docstring for Mpd
    """

    error_string = "Mpd: N/A"


    def __init__(self, config={}):
        """
        Constructor

        @param config:
            A map containing configuration parameters:

            "hostname":
                The mpd hostname to connect to. Default: "localhost"

            "port":
                The mpd port on the host. Default: 6600
        """
        super(Mpd, self).__init__()

        self.hostname = config.get("hostname", "localhost")
        self.port     = config.get("port", 6600)
        self.__is_connected = False

        if not mpd_lib_available:
            return

        self.mpdclient = mpd.MPDClient()
        self.mpdclient.timeout = 3


    def connect(self):
        if not mpd_lib_available or self.__is_connected:
            return

        try:
            self.mpdclient.connect("localhost", 6600)
            self.__is_connected = True

        except Exception as e:
            self.__is_connected = False


    def disconnect(self):
        if not self.__is_connected:
            return False

        self.mpdclient.disconnect()
        self.__is_connected = False


    def __del__(self):
        self.disconnect()


    def __status(self):
        mpd_status = self.mpdclient.status()

        state_string = None

        if "stop" == mpd_status["state"]:
            state_string = "■"
        elif "play" == mpd_status["state"]:
            state_string = "▶"
        elif "pause" == mpd_status["state"]:
            state_string = "▮▮"
        else:
            state_string = "(Unknown)"

        song_string = ""
        song_pos_string = ""

        if "songid" not in mpd_status or -1 == int(mpd_status["songid"]):
            song_string = "Mpd: No song"

        else:
            song_pos_string = "{}/{}".format \
                ( int(mpd_status["song"]) + 1
                , mpd_status["playlistlength"]
                )

            song = self.mpdclient.playlistid(mpd_status["songid"])[0]

            song_string = "{} - {}".format \
                (  *[s.decode("utf-8") if sys.version_info.major == 2 else s for s in \
                        [song.get(v) for v in ("artist", "title")]
                    ]
                )

        volume_string = ""
        volume = int(mpd_status["volume"])

        if -1 != volume:
            num_bars = 6
            num_full_bars = int(round(volume / (100. / num_bars)))

            volume_string = " {}{}".format \
                ( "▮" * num_full_bars
                , "▯" * (num_bars - num_full_bars)
                )

        return "{} ({}) {}{}".format \
            ( state_string
            , song_pos_string
            , song_string
            , volume_string
            )


    def status(self):
        if not self.__is_connected:
            self.connect()

            if not self.__is_connected:
                return Mpd.error_string

        try:
            return self.__status()

        except mpd.ConnectionError:
            self.__is_connected = False
            return Mpd.error_string


if __name__ == '__main__':
    import time
    m = Mpd()
    while True:
        print(m.status())
        time.sleep(1)
