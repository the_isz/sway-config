#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

class Time(object):
    """
    Docstring for Time
    """

    def __init__(self, config={}):
        """
        """
        super(Time, self).__init__()

        self.format = config.get("format", "%d.%m. %H:%M")


    def status(self):
        return time.strftime(self.format)
