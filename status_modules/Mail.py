# -*- coding: utf-8 -*-
# vim: set sw=4 et tw=79:

from __future__ import unicode_literals

import os.path
import mailbox
import sys
import collections


def mailbox_state(mbox, subdirs):
    """
    Scans the mailbox for new messages.

    @param mbox:
        The base directory of the mailbox.

    @param subdirs:
        An iterable containing either mail folder names or 2-tuples of mail
        folder name and display name.

    @return:
        A dictionary mapping the entries of the subdirs parameter to the number
        of new mails in that mailbox.
    """
    state = collections.OrderedDict()

    def to_maildir_fmt(paths):
        for path in iter(paths):
            yield path.rsplit(":")[0]

    for entry in subdirs:
        subdir = None
        display_name = None

        if list == type(entry):
            subdir, display_name = entry

        else:
            subdir, display_name = (entry, entry)

        path = os.path.join(mbox, subdir)
        maildir = mailbox.Maildir(path)
        state[display_name] = 0

        for file in to_maildir_fmt(os.listdir(os.path.join(path, "new"))):
            if file in maildir:
                state[display_name] += 1

    return state


def format_text(state, separator):
    """
    Convert the state of the mailboxes (as returned by mailbox_state) to a
    string.

    @param state: a dictionary as returned by mailbox_state.
    @return: a string representation of the given state.
    """
    items = state.iteritems() if sys.version < '3' else state.items()
    return separator.join("{} {}".format(*item) for item in items)



class Mail(object):
    """
    Docstring for Mail
    """

    def __init__(self, config={}):
        """
        Constructor

        @param config:
            A map containing configuration parameters:

            "separator":
                A string to put in between each mailbox entry. Default: "  "
                (two spaces)

            "mailboxes":
                A list defining which mailboxes to show. Each list entry must
                confirm to the following structure:

                [ <mailbox>, [ <subdir>, <subdir>, ... ] ]

                <mailbox>: Base directory of the mailbox, typically something
                    like "/home/user/Mail/gmail"

                <subdir>: Either a mail folder name (e.g. "INBOX") or a list of
                    the mail folder name and a string to use instead of it
                    (e.g. ["INBOX", "GMail"])

                The default is an empty list.
        """
        super(Mail, self).__init__()

        self.separator = config.get("separator", "  ")
        self.mailboxes = config.get("mailboxes", [])


    def status(self):
        return self.separator.join \
            ( format_text \
                ( mailbox_state \
                    ( mbox
                    , subdirs
                    )
                , self.separator
                ) for mbox, subdirs in self.mailboxes
            )


if __name__ == "__main__":
    import time
    m = Mail()
    while True:
        print(m.status())
        time.sleep(1)
