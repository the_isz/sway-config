#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os.path
import functools
 

def file_contents(base_path, file_name):
    with open(os.path.join(base_path, file_name), "r") as f:
        return f.read().strip()


class Battery(object):
    """
    Docstring for Battery
    """

    def __init__(self, config={}):
        """
        Constructor

        @param config: A map containing configuration parameters:
            "name" - The name of the directory containing battery information,
                     i.e. the part after "/sys/class/power_supply"
        """
        super(Battery, self).__init__()

        self.battery_name = config.get("name", "BAT0")


    def status(self):
        path_root = "/sys/class/power_supply/{}".format(self.battery_name)

        if not os.path.isdir(path_root):
            return "N.A."

        # From https://www.kernel.org/doc/Documentation/power/power_supply_class.txt:
        #
        # CHARGE_* attributes represents capacity in µAh only.
        # ENERGY_* attributes represents capacity in µWh only.
        # CAPACITY attribute represents capacity in *percents*, from 0 to 100.
        #
        # _NOW - momentary/instantaneous values.
        #
        # CHARGE_FULL_DESIGN, CHARGE_EMPTY_DESIGN - design charge values, when
        #     battery considered full/empty.
        #
        # ENERGY_FULL_DESIGN, ENERGY_EMPTY_DESIGN - same as above but for energy.

        fc = functools.partial(file_contents, path_root)

        status = fc("status")

        if status == "Full":
            return "⚡" # 

        capacity_prefix = None

        if os.path.isfile(os.path.join(path_root, "charge_now")):
            capacity_prefix = "charge"

        elif os.path.isfile(os.path.join(path_root, "energy_now")):
            capacity_prefix = "energy"

        else:
            return "N.A."

        capacity_full_design = int(fc("{}_full_design".format(capacity_prefix)))
        capacity_now         = int(fc("{}_now".format(capacity_prefix)))

        # "power_now" and "current_now" principally contain the same
        # information, but "current_now" is deprecated and may contain wrong
        # values, so use "power_now" whenever available.

        power_now = None

        if os.path.isfile(os.path.join(path_root, "power_now")):
            power_now = int(fc("power_now"))

        elif os.path.isfile(os.path.join(path_root, "current_now")):
            power_now = int(fc("current_now"))

        else:
            return "N.A."

        # power_now may be 0!?
        if power_now < 1:
            return "?"

        percent = int(round(float(capacity_now) / capacity_full_design * 100))
        minutes = 0
        symbol  = ""

        if status == "Discharging":
            minutes = int(round(float(capacity_now) / power_now * 60))
            symbol = "▼"

        if status == "Charging":
            minutes = int(round(float(capacity_full_design - capacity_now) / power_now * 60))
            symbol = "▲"

        return "{} {}% ({}:{:#02})".format(symbol, percent, minutes // 60, minutes % 60)


if __name__ == '__main__':
    import time
    battery = Battery()
    while True:
        print(battery.status())
        time.sleep(1)
