my sway (swaywm.org) configuration

Optional dependencies:

- alacritty (wayland terminal emulator)
- alacritty, fzf, bash (program launcher)
- mpc (console client for mpd)
- light (backlight manager)
- swayidle (idle manager to trigger DPMS)
- Hack (font)
- python2 or python3 (to generate status information via status.py)
- mpd2 python module (used in "Mpd" status_module)
